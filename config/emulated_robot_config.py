config = {
    'encoder_layers':[300],
    'decoder_layers':[300],
    'batch_size':16,
    'look_back':100,
    'look_ahead':50,
    'feature_in':2,# test with x first, 'v', 'w', 'x', 'z', 'pitch'
    'feature_out':2,#'x', 'z', 'pitch'
    'droprate':0.25,
    'loss':'mean_squared_error',
    'learning_rate':0.005,
    'bidirection':False,
    'lstm_inversed':False,
}