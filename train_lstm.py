""" Author: Mao Ngo
Created data: 2020-03-17
"""
from __future__ import division

import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"
import numpy as np
import pickle
import math
from keras.models import load_model
import logging
import importlib
import argparse
import time
from matplotlib import pyplot
from scipy.stats import multivariate_normal

import model.lstm_seq2seq as lstm_seq2seq
import tensorflow as tf
import keras.backend as K
from sklearn.model_selection import TimeSeriesSplit
import utils.facilities as facilities

def get_flops(model):
    run_meta = tf.compat.v1.RunMetadata()
    opts = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()

    # We use the Keras session graph in the call to the profiler.
    flops = tf.compat.v1.profiler.profile(graph=K.get_session().graph,
                                run_meta=run_meta, cmd='op', options=opts)

    return flops.total_float_ops  # Prints the "flops" of the model.
    
def plot_training_history(history, dataset, is_show=False):
    pyplot.title('Loss / Mean Absolute Error')
    pyplot.plot(history.history['loss'], label='train')
    pyplot.plot(history.history['val_loss'], label='test')
    pyplot.legend()
    pyplot.savefig('trained_model/{}/train_history.png'.
        format(dataset), format='png', dpi=1000,
        bbox_inches="tight")
    if is_show:
        pyplot.show()
        
def train_and_test(args):
    cfg = importlib.import_module('config.{}_config'.format(args.dataset)).config
    logging.info(cfg)
    data = importlib.import_module('Dataset.{}'.format(args.dataset))
    train_input_scaler, train_output_scaler, train_input, train_target,\
        test_input, test_target = \
            data.get_data(cfg['look_back'], cfg['look_ahead'])
    
    base_model = lstm_seq2seq
    START = np.full((1, cfg['feature_out']), 0)
    
    decoder_input = train_target
    decoder_output = train_target
    # padding START to the begin of each decoder_input window,
    # (and STOP to the end of each decoder_output window, if we need STOP)
    new_decoder_input = []
    for i, d_i in enumerate(decoder_input):
        # append a special token START at the beginning, and remove the last decoder input
        #print("i={}, START={}, d_i={}".format(i, START.shape, d_i.shape))
        n_d_i = np.vstack((START, d_i[:-1]))
        new_decoder_input.append(n_d_i)
    decoder_input = np.asarray(new_decoder_input)
    logging.info("train_input={}, train_target={}, decoder_input={}, decoder_output={}".\
        format(train_input.shape, train_target.shape, decoder_input.shape, decoder_output.shape))
    
    generator = base_model.LstmSeq2Seq(cfg['encoder_layers'], cfg['decoder_layers'],
        cfg['look_back'], cfg['look_ahead'], cfg['feature_in'], cfg['feature_out'],
        cfg['droprate'], cfg['loss'], cfg['learning_rate'], START, cfg['bidirection'])
        
    model, encoder_model, decoder_model = generator.build_model()
    logging.info("Total flops={}".format(get_flops(model)))
    
    # TRAIN the model
    num_folds=5
    tscv = TimeSeriesSplit()
    for train_id, test_id in tscv.split(train_input):
        train_in, test_in = train_input[train_id], train_input[test_id]     
        decoder_in, test_decoder_in = decoder_input[train_id], decoder_input[test_id]
        decoder_ou, test_decoder_ou = decoder_output[train_id], decoder_output[test_id]
        history = model.fit([train_in, decoder_in], decoder_ou, 
            batch_size=cfg['batch_size'],
            epochs=args.nb_epochs,
            shuffle=False,
            validation_data=([test_in, test_decoder_in], test_decoder_ou),
            verbose=args.verbose*1)
        logging.info("Avg train loss={}, avg val_loss={}".
            format(np.mean(history.history['loss']),
                   np.mean(history.history['val_loss']) ) )
        
    # Predicting/testing
    decoded_train = predict_sequence_data(train_input, generator, cfg)
    decoded_test = predict_sequence_data(test_input, generator, cfg)
    logging.debug("decoded_train={}, decoded_test={}".format(decoded_train.shape, decoded_test.shape))
    
    # Flattening look_back, and look_ahead
    train_input = facilities.flatten_look_back_seq(train_input, look_back=5, step=1)
    test_input = facilities.flatten_look_back_seq(test_input, look_back=5, step=1)
    logging.debug("train_input={},test_input={}".
        format(train_input.shape, test_input.shape))   
    
    decoded_train = facilities.flatten_look_ahead_seq(decoded_train, look_ahead=1)
    decoded_test = facilities.flatten_look_ahead_seq(decoded_test, look_ahead=1)
    train_target = facilities.flatten_look_ahead_seq(train_target, look_ahead=1)
    test_target = facilities.flatten_look_ahead_seq(test_target, look_ahead=1)
    logging.debug("After flattening the input and targets")
    logging.debug("train_target={}, test_target={}, decoded_train={}, decoded_test={}".
        format(train_target.shape,
            test_target.shape, decoded_train.shape, decoded_test.shape))
    
    # Rescale back to original range before calculating errors
    train_input = train_input_scaler.inverse_transform(train_input)
    test_input = train_input_scaler.inverse_transform(test_input)
    
    test_target = train_output_scaler.inverse_transform(test_target)
    train_target = train_output_scaler.inverse_transform(train_target)
    decoded_train = train_output_scaler.inverse_transform(decoded_train)
    decoded_test = train_output_scaler.inverse_transform(decoded_test)
        
    logging.debug("train_target={}, test_target={}, decoded_train={}, decoded_test={}".format(train_target.shape, test_target.shape, decoded_train.shape, decoded_test.shape))
    
    # Saving trained models and data
    dir ='trained_model/{}'.format(args.dataset)
    if not os.path.exists(dir):
        os.makedirs(dir)

    result_dir = 'results/{}'.format(args.dataset)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
        
    if args.store_result:
        logging.info("Save whole model: {}/{}.h5".format(dir, args.model))
        model.save('{}/{}.h5'.format(dir, args.model))
        logging.info('Save encoder: {}/{}_encoder.h5'.format(dir, args.model))
        encoder_model.save('{}/{}_encoder.h5'.format(dir, args.model))
        logging.info('Save decoder: {}/{}_decoder.h5'.format(dir, args.model))
        decoder_model.save('{}/{}_decoder.h5'.format(dir, args.model))
        # save decoded data after rescaling:
        train_input.dump('{}/train_input.npk'.format(result_dir))
        train_target.dump('{}/train_target.npk'.format(result_dir))
        test_input.dump('{}/test_input.npk'.format(result_dir))
        test_target.dump('{}/test_target.npk'.format(result_dir))
        decoded_train.dump('{}/decoded_train.npk'.format(result_dir))
        decoded_test.dump('{}/decoded_test.npk'.format(result_dir))
    
    # EVALUATION Performance
    error_train = np.abs(decoded_train - train_target) # no_windows x feature_out
    error_test = np.abs(decoded_test - test_target)
    
    logging.debug("error_train={}, error_test={}".format(
        error_train.shape, error_test.shape))
        
    mu = np.mean(error_train, axis=0)
    sigma = np.cov(error_train, rowvar=False)
    logging.info("mu={}, sigma={}".format(mu, sigma.shape))
    logPD_train = multivariate_normal.logpdf(error_train, mu, sigma)
    threshold = np.percentile(logPD_train, 0.05)
    logPD_test = multivariate_normal.logpdf(error_test, mu, sigma)
    pred_test = (logPD_test < threshold) * 1.
    ### TODO: we need to generate a synthesized test data with anomalies
    Y_test = np.zeros((pred_test.shape))
    logging.info("pred_test={}".format(pred_test.shape))
    results = facilities.evaluate_pred_truth(pred_test, Y_test)
    logging.info("Eval results over point={}".format(results))
    
    if args.plot:
        plot_training_history(history, args.dataset)
        
    
def predict_sequence_data(X, generator, cfg):
    decoded_data = None
    for i in range(len(X)):
        x_i = X[i: i+1]
        #print("x_i={}".format(x_i.shape))
        decoded_ = generator.decode_sequence(x_i)
        if cfg['lstm_inversed']:
            # reverse again
            decoded_ = decoded_[:,::-1,:]
        if decoded_data is None:
            decoded_data = decoded_
        else:
            decoded_data = np.concatenate((decoded_data, decoded_), axis=0)
    decoded_data = np.asarray(decoded_data)
    logging.debug("decoded_data.shape={}".format(decoded_data.shape))
    return decoded_data

def test_lstm_seq2seq(args):
    pass
    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--verbose',
        action='store_true')
    parser.add_argument(
        '--test',
        action='store_true')
    parser.add_argument(
        '--dataset',
        type=str,
        help='Experiment dataset: emulated_robot. Default is emulated_robot.',
        default='emulated_robot')
    parser.add_argument(
        '--nb_epochs',
        help='Number of epochs for training. Default is 2.',
        type=int,
        default=2)
    parser.add_argument(
        '--batch_size',
        type=int,
        help='Batch size. Default is 16',
        default=16)
    parser.add_argument(
        '--look_back',
        type=int,
        help='Number of look-back time-steps. Default is 5.',
        default=5)
    parser.add_argument(
        '--look_ahead',
        type=int,
        help='Number of look-ahead time-steps. Default is 1.',
        default=1)
    parser.add_argument(
        '--model',
        type=str,
        help='Model name. Default is lstm_seq2seq',
        default='lstm_seq2seq')
    parser.add_argument(
        '--plot',
        action='store_true',
        help='Enable plot graph if running with Xserver.')
    parser.add_argument(
        '--store_result',
        action='store_true',
        help='Enable to store input and reconstructed results of valid and test sets.')
    
    args = parser.parse_args()
    mode = 'testing' if args.test else 'training'

    FILENAME = 'logfiles/{}/{}_{}.log'.format(args.dataset, mode, args.model)
    if not os.path.isdir('logfiles/{}'.format(args.dataset)):
        os.makedirs('logfiles/{}'.format(args.dataset))
        
    if args.verbose:
        LOG_LVL = logging.DEBUG
    else:
        LOG_LVL = logging.INFO
    
    FORMAT = '%(asctime)-15s %(levelname)s %(filename)s %(lineno)s:: %(message)s'    

    logging.getLogger('matplotlib.font_manager').disabled = True

    fileHandler = logging.FileHandler(FILENAME, mode='w')
    fileHandler.setFormatter(logging.Formatter(FORMAT))

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logging.Formatter(FORMAT))

    logger = logging.getLogger('')
    if logger.hasHandlers():
        logger.handlers.clear()
    logger.addHandler(consoleHandler)
    logger.addHandler(fileHandler)
    logger.setLevel(LOG_LVL)

    if args.test:
        test_lstm_seq2seq(args)
    else:
        logging.info("run train_and_test()")
        train_and_test(args)