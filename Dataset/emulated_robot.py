import pandas as pd
import time as time
import numpy as np
from sklearn.preprocessing import StandardScaler
import logging


# Apply only for training data,
# then use returned scaler object to scale test data
def standardize(data):
    scaler = StandardScaler()
    print("shape data={}".format(data.shape))
    data = scaler.fit_transform(data)
    print("shape transformed data={}".format(data.shape))
    print("Scaler Mean: {}".format(scaler.mean_))
    print("Scaler Variance: {}".format(scaler.var_))
    print("data mean {}, data variance {}".format(np.mean(data, axis=0),np.var(data, axis=0)))
    return data, scaler

def prepare_seq2seq_data(input_dataset, output_dataset, look_back, look_ahead):
    # create a fake look_back+look_ahead at the tail
    #logging.debug("dataset.shape={}".format(dataset.shape))
    #dataset = np.vstack((dataset, dataset[-(look_back+look_ahead):]))
    logging.debug("input_dataset={}, output_dataset={}".format(
        input_dataset.shape, output_dataset.shape))
    data_input, data_target = [], []
    for i in range(len(input_dataset) - look_back - look_ahead):
        input_seq = input_dataset[i:(i + look_back)]
        output_seq = output_dataset[i + look_back:(i + look_back + look_ahead)]
        data_input.append(input_seq)
        data_target.append(output_seq)
    data_input = np.array(data_input)
    data_target = np.array(data_target)
    logging.debug("data_input.shape={}, data_target.shape={}".format(
        data_input.shape, data_target.shape))
    return data_input, data_target

def get_raw_data():
    # Load data from csv
    try:
        start = time.time()
        logging.info("Load preprocessed data from npy files."\
            "Delete npy files when you change the feature set.")
        train_input_set = np.load('Dataset/emulated_robot/train_input_set.npy')
        train_output_set = np.load('Dataset/emulated_robot/train_output_set.npy')
        test_input_set = np.load('Dataset/emulated_robot/test_input_set.npy')
        test_output_set = np.load('Dataset/emulated_robot/test_output_set.npy')
        logging.info("load Dataset from npy files, take={}".format(time.time() - start))
    except IOError:
        start = time.time()
        df = pd.read_csv('Dataset/emulated_robot/data_log.csv')
        logging.info("load Dataset from csv files, take={}".format(time.time()-start))
        # v and w are control signal
        # x,z,pitch are imu and position
        #features = ['v', 'w', 'x', 'z', 'pitch']
        input_features = ['x','z']
        output_features = ['x','z']
        in_dataset = df[input_features]
        out_dataset = df[output_features]
        logging.info("in_dataset={}, out_dataset={}".format(in_dataset.shape,
            out_dataset.shape))
        len_data =len(in_dataset) # two datasets have the same length.
        ratio_train_test=0.7
        train_input_set = in_dataset[:int(ratio_train_test*len_data)]
        train_output_set = out_dataset[:int(ratio_train_test*len_data)]
        test_input_set = in_dataset[int(ratio_train_test*len_data):]
        test_output_set = out_dataset[int(ratio_train_test*len_data):]
        np.save('Dataset/emulated_robot/train_input_set.npy', train_input_set)
        np.save('Dataset/emulated_robot/train_output_set.npy', train_output_set)
        np.save('Dataset/emulated_robot/test_input_set.npy', test_input_set)
        np.save('Dataset/emulated_robot/test_output_set.npy', test_output_set)
        logging.info("dumped csv files to npy files.")

    # standardize train set and transform test set
    train_input_data, train_input_scaler = standardize(train_input_set)
    test_input_data = train_input_scaler.transform(test_input_set)
    train_output_data, train_output_scaler = standardize(train_output_set)
    test_output_data = train_output_scaler.transform(test_output_set)
    logging.info("return train_input_data={}, test_input_data={}".format(
        train_input_data.shape, test_input_data.shape))
    logging.info("return train_output_data={}, test_output_data={}".format(
        train_output_data.shape, test_output_data.shape))
    return train_input_data, test_input_data, train_input_scaler, \
           train_output_data, test_output_data, train_output_scaler


def get_data(look_back, look_ahead):
    train_input_data, test_input_data, train_input_scaler, \
        train_output_data, test_output_data, train_output_scaler = get_raw_data()
    
    train_input, train_target = prepare_seq2seq_data(train_input_data, train_output_data,
                                    look_back, look_ahead)
    test_input, test_target = prepare_seq2seq_data(test_input_data, test_output_data,
                                    look_back, look_ahead)

    return train_input_scaler, train_output_scaler, train_input, train_target,\
           test_input, test_target
