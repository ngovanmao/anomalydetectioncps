from __future__ import division
import os
import csv
import numpy as np
from sklearn.metrics import roc_curve, auc, precision_recall_fscore_support
from sklearn.metrics import confusion_matrix

def flatten_look_back_seq(data, look_back=5, step=1):
    """
    Input: data is sliding widnows matrix of input sequence with following format:
    a11 a12 a13 a14 a15
        a21 a22 a23 a24 a25 
            a31 a32 a33 a34 a35
                a41 a41 a43 a44 a45
                    a51 a52 a53 a54 a55
                        a61 a62 a63 a64 a65
    Expected output of the original input before sliding window:
    output: if step=1
    a11 a21 a31 a41 ...an1
    """
    output = None
    for d in data:
        o = d[:step]
        if output is None:
            output = o
        else:
            output = np.concatenate((output, o), axis=0)
    print("output.shape={}".format(output.shape))        
    return output
    
#get all diagonals from a two d array
def get_diagonals(input):
    #fetch diagonals in a list
    diagonals = [input[::-1,:,:].diagonal(i) for i in range(-input.shape[0] + 1, 1)]
    return diagonals
    
def flatten_look_ahead_seq(data, look_ahead=5):
    """ Description: data matrix: (suppose look ahead =5)
    a11 a12 a13 a14 a15
    a21 a22 a23 a24 a25
    a31 a32 a33 a34 a35
    a41 a42 a43 a44 a45
    a51 a52 a53 a54 a55
    a61 a62 a63 a64 a65

  ==> take a diagonal from the bottom left corner to the upper right corner
    [a61]
    [a51 a62]
    [a41 a52 a63]
    [a31 a42 a53 a64]
    [a21 a31 a43 a54 a65]
    [a11 a22 a33 a44 a55]
    ...
    [a15]

  ==> Fill unfinished rows with the first element:
    [a61 a61 a61 a61 a61]
    [a51 a62 a51 a51 a51]
    [a41 a52 a63 a41 a41]
    [a31 a42 a53 a64 a31]
    [a21 a31 a43 a54 a65]
    [a11 a22 a33 a44 a55]
    ....
    [a15 a15 a15 a15 a15]
  ==> I decided to take np.mean for each row:
    np.mean(diagonals, axis=1)
    """
    if data.shape[1] > 1: # more than 1 look_ahead
        # diagonals contains a reading's values calculated at different points in time
        diagonals = get_diagonals(data)
        # the top left and bottom right predictions do not contain predictions for all timesteps
        # fill the missing prediction values in diagonals. curenttly using the first predicted value for all missing timesteps
        for idx, diagonal in enumerate(diagonals):
            diagonal = diagonal.flatten()
            # missing value filled with the first value
            diagonals[idx] = np.hstack((diagonal, np.full(look_ahead - len(diagonal), diagonal[0])))

        # original: data_timesteps = np.asarray(diagonals)
        data_timesteps = np.asarray(diagonals)
        data_timesteps = data_timesteps[:,0]
        #data_timesteps = np.mean(data_timesteps, axis=1)
    else:
        data_timesteps = np.squeeze(data)
    return data_timesteps
    
    
def evaluate_pred_truth(y_pred, y_truth, mes=''):
    results = {}

    total_sample = np.product(y_pred.shape)
    correct_sample = np.sum(y_pred == y_truth)
    accuracy = correct_sample/total_sample
    if accuracy == 1.0:
        tn = float(np.sum(y_pred == 0))
        fp = 0.
        fn = 0.
        tp = float(np.sum(y_pred == 1))
    else:
        tn, fp, fn, tp = confusion_matrix(y_truth.astype(int),
        y_pred.astype(int)).ravel()

    accuracy2 = (tp + tn) /(tp+tn+fp+fn)
    assert accuracy == accuracy2
    if fp + tn == 0:
        prob_false_alarm = 0.0
    else:
        prob_false_alarm = fp/(fp+tn)
    precision, recall, f1, _ = precision_recall_fscore_support(
        y_truth.astype(int), y_pred.astype(int), average='binary')

    results = dict(tn=float(tn), fp=float(fp), fn=float(fn), tp=float(tp),
        accuracy=accuracy, prob_false_alarm=prob_false_alarm,
        precision=precision, recall=recall, f1=f1)
    return results
