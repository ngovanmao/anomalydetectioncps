from __future__ import division

import time
import numpy as np
from keras.layers import Input, Dense, Dropout, RepeatVector
from keras.layers import LSTM as myLSTM
# Only use CuDNNLSTM for training Cloud model.
#from keras.layers import CuDNNLSTM as myLSTM

from keras.layers import TimeDistributed, Bidirectional, Concatenate
from keras.models import Model, Sequential
from keras.optimizers import RMSprop
from keras.regularizers import l2
import logging
import random


class LstmSeq2Seq(object):
    """ LstmSeq2Seq is to encode an input sequence into a encoded_states=[state_h, state_c].
    Then the first symbol is decoded based on the START token and encoded_states, and then
    subsequent symbol is decoded based on the previous symbol and the previous states.
    Here the training phase is different from inference phase, we build two
    different decoder for the two purposes.
    """
    def __init__(self, encoder_layers, decoder_layers,
        n_look_back, n_look_ahead, n_feature_in, n_feature_out,
        dropout, loss, learning_rate, START, is_bidirection=False):

        self.encoder_layers = encoder_layers
        self.decoder_layers = decoder_layers
        self.n_look_back = n_look_back
        self.n_look_ahead = n_look_ahead
        self.n_feature_in = n_feature_in
        self.n_feature_out = n_feature_out
        self.loss = loss
        self.learning_rate = learning_rate
        self.dropout = dropout
        self.START = START
        logging.info("LSTM Seq2Seq model Info: %s" % (locals()))
        self.is_bidirection=is_bidirection

    def build_model(self):

        #### ENCODER:
        encoder_inputs = Input(shape=(None, self.n_feature_in))
        logging.info("encoder_inputs.shape={}".format(encoder_inputs.get_shape()))
        encoder_states = []
        max_len = len(self.encoder_layers)
        if max_len > 1:
            return_sequences = True
        else:
            return_sequences = False
        e_inputs = encoder_inputs
        if self.is_bidirection:
            for i, layer_dim in enumerate(self.encoder_layers):
                e_outputs, ef_h, ef_c, eb_h, eb_c =\
                    Bidirectional(myLSTM(layer_dim,
                                    kernel_regularizer=l2(1e-4),
                                    return_sequences=return_sequences,
                                    return_state=True))(e_inputs)

                state_h = Concatenate()([ef_h, eb_h])
                state_c = Concatenate()([ef_c, eb_c])
                encoder_states.append(state_h)
                encoder_states.append(state_c)
                if i < max_len - 1:
                    e_outputs = Dropout(self.dropout)(e_outputs)
                    e_inputs = e_outputs
                    return_sequences = True
                else:
                    return_sequences=False
        else:
            for i, layer_dim in enumerate(self.encoder_layers):
                e_outputs, ehi, eci = myLSTM(layer_dim,
                                        kernel_regularizer=l2(1e-3),
                                        return_sequences=return_sequences,
                                        return_state=True)(e_inputs)
                encoder_states.append(ehi)
                encoder_states.append(eci)
                if i < max_len - 1:
                    e_outputs = Dropout(self.dropout)(e_outputs)
                    e_inputs = e_outputs
                    return_sequences = True
                else:
                    return_sequences=False

        ### DECODER: set up the decoder, using `encoder_states` as initial state.
        decoder_inputs = Input(shape=(None, self.n_feature_out))
        # We set up our decoder to return full output sequences,
        # and to return internal states as well. We don't use the
        # return states in the training model, but we will use them in inference.
        initial_states = encoder_states
        d_inputs = decoder_inputs
        max_len_decoder = len(self.decoder_layers)
        decoder_states_inputs = [] # for define Model later
        decoder_lstms = []
        for i, layer_dim in enumerate(self.decoder_layers):
            if self.is_bidirection:
                factor = 2
            else:
                factor = 1
            decoder_lstm = myLSTM(layer_dim * factor,
                                    kernel_regularizer=l2(1e-4),
                                    return_sequences=True,
                                    return_state=True)
            d_outputs, dhi, dci = decoder_lstm(d_inputs,
                                    initial_state=initial_states)
            initial_states = [dhi, dci]
            # input of the higher layer is the output of the previously lower layer
            d_inputs = d_outputs
            d_outputs = Dropout(self.dropout)(d_outputs)

            # For definition Model later
            d_input_h = Input(shape=(layer_dim*factor, ))
            d_input_c = Input(shape=(layer_dim*factor, ))
            decoder_states_inputs.append(d_input_h)
            decoder_states_inputs.append(d_input_c)
            decoder_lstms.append(decoder_lstm)

        decoder_dense = Dense(self.n_feature_out, activation='linear')
        decoder_outputs = decoder_dense(d_outputs)

        ### Define the model that will be saved
        # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
        self.model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
        # Encoder Model
        self.encoder_model = Model(encoder_inputs, encoder_states)
        # Decoder Model
        de_inputs = decoder_inputs
        de_states_outputs = []
        for i, layer_dim in enumerate(self.decoder_layers):
            decoder_lstm = decoder_lstms[i]
            de_states_inputs = decoder_states_inputs[i:i+2]
            de_outputs, state_h, state_c = decoder_lstm(de_inputs,
                                    initial_state=de_states_inputs)
            de_states_outputs += [state_h, state_c]
            de_inputs = de_outputs

        de_outputs = decoder_dense(de_outputs)
        self.decoder_model = Model([decoder_inputs] + decoder_states_inputs,
                                    [de_outputs] + de_states_outputs)


        # Run training
        self.model.compile(optimizer=RMSprop(self.learning_rate),
                           loss=self.loss,
                           metrics=['accuracy'])

        logging.info(self.model.summary())

        return self.model, self.encoder_model, self.decoder_model

    def encoded_state(self, input_seq):
        encoded_states = self.encoder_model.predict(input_seq)
        return encoded_states

    def decode_sequence(self, input_seq):
        # Encode the input as state vectors.
        encoded_states = self.encoder_model.predict(input_seq)
        START = 0
        target_seq = np.full((1, 1, self.n_feature_out), START)
        #print("target_seq={}".format(target_seq))
        # Sampling loop for a batch of sequences
        # (to simplify, here we assume a batch of size 1).
        stop_condition = False
        decoded_sequences = None
        states_value = encoded_states
        #print("states_values={}".format(states_value))
        while not stop_condition:
            #output_value, h, c = self.decoder_model.predict(
            outputs = self.decoder_model.predict(
                    [target_seq] + states_value)
            output_value = outputs[0]
            states_value = outputs[1:]
            #states_value = [h, c]
            if decoded_sequences is None:
                decoded_sequences = output_value
            else:
                decoded_sequences = np.concatenate((decoded_sequences,
                                                    output_value), axis=1)

            # Exit condition: either hit max length
            # or find stop character.
            if decoded_sequences.shape[1] > (self.n_look_ahead - 1):
                stop_condition = True

            # Update the target sequence (of length 1).
            target_seq = output_value

        decoded_sequences = np.asarray(decoded_sequences)

        return decoded_sequences